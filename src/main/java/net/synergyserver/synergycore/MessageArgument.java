package net.synergyserver.synergycore;

/**
 * Represents an argument passed into <code>Message#format</code>.
 */
public class MessageArgument {

    private int startingIndex;
    private int endingIndex;
    private String text;

    /**
     * Creates a new <code>MessageArgument</code> with the given parameters.
     *
     * @param startingIndex The index of the start of the placeholder in the message that this argument is being substituted into.
     * @param endingIndex The index of the end of the placeholder in the message that this argument is being substituted into.
     * @param text The text of this argument.
     */
    public MessageArgument(int startingIndex, int endingIndex, String text) {
        this.startingIndex = startingIndex;
        this.endingIndex = endingIndex;
        this.text = text;
    }

    /**
     * Gets the index of the start of the placeholder in the message that this argument is being substituted into.
     *
     * @return The starting index of this argument.
     */
    public int getStartingIndex() {
        return startingIndex;
    }

    /**
     * Sets the index of the start of the placeholder in the message that this argument is being substituted into.
     *
     * @param startingIndex The new starting index of this argument.
     */
    public void setStartingIndex(int startingIndex) {
        this.startingIndex = startingIndex;
    }

    /**
     * Gets the index of the end of the placeholder in the message that this argument is being substituted into.
     *
     * @return The ending index of this argument.
     */
    public int getEndingIndex() {
        return endingIndex;
    }

    /**
     * Sets the index of the end of the placeholder in the message that this argument is being substituted into.
     *
     * @param endingIndex The new ending index of this argument.
     */
    public void setEndingIndex(int endingIndex) {
        this.endingIndex = endingIndex;
    }

    /**
     * Gets the text of this argument.
     *
     * @return The text of this argument.
     */
    public String getText() {
        return text;
    }
}
