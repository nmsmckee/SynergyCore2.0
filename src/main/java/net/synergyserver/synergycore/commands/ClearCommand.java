package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "clear",
        aliases = {"clearinventory", "clearinv", "ci"},
        permission = "syn.clear",
        usage = "/clear [player] [-hotbar]",
        description = "Clears the inventory of yourself or another player.",
        maxArgs = 1,
        parseCommandFlags = true
)
public class ClearCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        // If the sender is trying to clear their own inventory
        if (args.length == 0) {
            // Give an error if the sender isn't a player
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                sender.sendMessage(Message.get("commands.error.wrong_sender_type"));
                return false;
            }

            Player player = (Player) sender;

            if (flags.hasFlag("-hotbar", "-hb", "-h")){
                for (int i = 0; i < 9; i++) {
                    player.getInventory().clear(i);
                }
                player.sendMessage(Message.get("commands.clear.info.hotbar_cleared"));
            } else {
                player.getInventory().clear();
                player.sendMessage(Message.get("commands.clear.info.inv_cleared"));
            }

            return true;
        }

        // Give an error if the sender doesn't have permission to open another player's inventory
        if (!sender.hasPermission("syn.clear.others")) {
            sender.sendMessage(Message.get("commands.clear.error.no_permission_others"));
            return false;
        }

        UUID targetID = PlayerUtil.getUUID(args[0], false, sender.hasPermission("vanish.see"));

        // If no player was found then give the sender an error message
        if (targetID == null) {
            sender.sendMessage(Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // Clear the inventory or hotbar of the requested player
        if (flags.hasFlag("-hotbar", "-hb", "-h")){
            for (int i = 0; i < 9; i++){
                Bukkit.getPlayer(targetID).getInventory().clear(i);
            }
            sender.sendMessage(Message.format("commands.clear.info.hotbar_cleared_others", PlayerUtil.getName(targetID)));
        } else {
            Bukkit.getPlayer(targetID).getInventory().clear();
            sender.sendMessage(Message.format("commands.clear.info.inv_cleared_others", PlayerUtil.getName(targetID)));
        }


        return true;
    }
}
