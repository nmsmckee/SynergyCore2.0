package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "socialspy",
        aliases = {"ss", "spy"},
        permission = "syn.socialspy",
        usage = "/socialspy <add|remove|list>",
        description = "Main command for managing spied players that you are watching all messages of.",
        validSenders = SenderType.PLAYER
)
public class SocialSpyCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
