package net.synergyserver.synergycore.guis;

import net.synergyserver.synergycore.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * An enum that contains all the miscellaneous GUI items.
 */
public enum GUIItem implements Itemizable {
    PRESET_INFO("Click on a preset to load it.", "To manage your presets, do " + ChatColor.WHITE + "/settings preset", Material.OAK_SIGN);

    private String displayName;
    private String description;
    private ItemStack item;

    GUIItem(String displayName, String description, Material itemType) {
        this.displayName = displayName;
        this.description = description;
        this.item = new ItemStack(itemType, 1);
    }


    public String getDisplayName() {
        return displayName;
    }

    public String getDescription() {
        return description;
    }

    /**
     * Gets the item of this <code>GUIItem</code> to use in an item-based GUI.
     *
     * @return The item of this <code>GUIItem</code>.
     */
    public ItemStack getItem() {
        return item;
    }

    public ItemStack getItem(Object parameter) {
        ItemStack item = getItem();

        ItemMeta im = item.getItemMeta();
        List<String> lore = new ArrayList<>();

        im.setDisplayName(ChatColor.WHITE + getDisplayName());
        lore.add(ChatColor.GRAY + getDescription());

        im.setLore(lore);
        item.setItemMeta(im);
        return item;
    }
}
