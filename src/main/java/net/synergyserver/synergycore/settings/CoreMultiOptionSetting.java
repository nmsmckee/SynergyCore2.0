package net.synergyserver.synergycore.settings;

import org.bukkit.Material;

/**
 * Represents a <code>MultiOptionSetting</code> handled by SynergyCore.
 */
public class CoreMultiOptionSetting extends MultiOptionSetting {

    public static final CoreMultiOptionSetting AUTO_TPACCEPT = new CoreMultiOptionSetting("auto_tpaccept",
            SettingCategory.TELEPORTATION, "Automatically accepts all teleport requests where the player wishes to " +
            "teleport to you.", "syn.setting.auto-tpaccept", "none", "none", new SettingOption[]{
            new SettingOption("all", "syn.setting.auto-tpaccept.all", "Accepts both /tpr and /tprhere requests.",
                    Material.CHORUS_FLOWER),
            new SettingOption("tpr_only", "syn.setting.auto-tpaccept.tpr-only",
                    "Only automatically accepts teleport requests where the sender wishes to teleport to you.",
                    Material.CHORUS_FRUIT),
            new SettingOption("none", "syn.setting.auto-tpaccept.none",
                    "Does not automatically accept any teleport requests.", Material.POPPED_CHORUS_FRUIT)
    });

    public static final CoreMultiOptionSetting DEATH_MESSAGES = new CoreMultiOptionSetting("death_messages",
            SettingCategory.CHAT, "Changes the visibility of death messages.", "syn.setting.death-messages",
            "all", "all", new SettingOption[]{
            new SettingOption("all", "syn.setting.death-messages.all",
                    "Shows all death messages, regardless of where the death occurred.", Material.WITHER_SKELETON_SKULL),
            new SettingOption("same_world_group", "syn.setting.death-messages.same-world-group",
                    "Shows only the death messages that occur in the same world group as you.", Material.SKELETON_SKULL),
            new SettingOption("none", "syn.setting.death-messages.none", "Hides all death messages.",
                    Material.PLAYER_HEAD)
    });
    public static final CoreMultiOptionSetting NAME_MENTIONING = new CoreMultiOptionSetting("name_mentioning",
            SettingCategory.CHAT, "Changes the method used to detect your name in chat.", "syn.setting.name-mentioning",
            "automatic", "automatic", new SettingOption[]{
            new SettingOption("automatic", "syn.setting.name-mentioning.automatic",
                    "Uses Synergy's patented String#contains() method of detecting names.", Material.WATER_BUCKET),
            new SettingOption("automatic_with_blacklist", "syn.setting.name-mentioning-blacklist",
                    "Uses the automatic method of detecting names but prevents elements in your " +
                            "name_mentioning_blacklist setting from being detected as your name.", Material.BLACK_STAINED_GLASS_PANE),
            new SettingOption("whitelist", "syn.setting.name-mentioning-whitelist",
                    "Only words that match the elements in your name_mentioning_whitelist setting will be detected as your name.",
                    Material.PAPER)
    });

    public static final CoreMultiOptionSetting VANISH_BEHAVIOR = new CoreMultiOptionSetting(
            "vanish_behavior", SettingCategory.SECRET, "Alters the default behavior of /vanish.",
            "syn.setting.vanish-behavior", "fake_messages", "fake_messages", new SettingOption[]{
            new SettingOption("fake_messages", "syn.setting.vanish-behavior.fake_messages",
                    "Fakes join and leave messages whenever you use the command.", Material.BIRCH_DOOR),
            new SettingOption("silent", "syn.setting.vanish-behavior.silent",
                    "No messages are sent when you use the command.", Material.GLASS_PANE),
            new SettingOption("legacy", "syn.setting.vanish-behavior.legacy", "The legacy behavior, which " +
                    "is to only send a fake join message on your first use of the command after you log in.",
                    Material.STONE_BRICKS)
    });

    private CoreMultiOptionSetting(String id, SettingCategory category, String description, String permission,
                                  String defaultValue, String defaultValueNoPermission, SettingOption[] options) {
        super(id, category, description, permission, defaultValue, defaultValueNoPermission, options);
    }
}
