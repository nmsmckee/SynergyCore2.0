package net.synergyserver.synergycore.settings;

import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.LinkedHashSet;

/**
 * Represents a <code>SettingManager</code>, which registers and manages <code>Setting</code>s.
 */
public class SettingManager {

    private static SettingManager instance;
    private HashMap<String, Setting> settings;

    /**
     * Creates a new <code>SettingManager</code> object.
     */
    private SettingManager() {
        this.settings = new HashMap<>();
    }

    /**
     * Returns the object representing this <code>SettingManager</code>.
     *
     * @return The object of this class.
     */
    public static SettingManager getInstance() {
        if (instance == null) {
            instance = new SettingManager();
        }
        return instance;
    }

    /**
     * Registers the constants of a class that implements the <code>Setting</code> interface.
     *
     * @param clazz The class of the <code>Setting</code> to register.
     */
    public void registerSettings(Class<? extends Setting> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) {
            return;
        }
        if (Modifier.isInterface(clazz.getModifiers())) {
            return;
        }
        if (!Modifier.isPublic(clazz.getModifiers())) {
            return;
        }

        for (Field field : clazz.getDeclaredFields()) {
            if (!Modifier.isStatic(field.getModifiers())) {
                continue;
            }
            if (!Modifier.isFinal(field.getModifiers())) {
                continue;
            }
            if (!Modifier.isPublic(field.getModifiers())) {
                continue;
            }
            if (!field.getType().equals(clazz)) {
                continue;
            }

            try {
                Setting setting = (Setting) field.get(null);
                settings.put(setting.getIdentifier(), setting);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Gets all <code>Setting</code>s registered, with their identifiers as keys.
     *
     * @return All registered <code>Setting</code>s.
     */
    public HashMap<String, Setting> getSettings() {
        return settings;
    }

    /**
     * Gets a LinkedHashSet containing a list of <code>Setting</code>s that the given player can use at that moment.
     *
     * @param player The player to check permissions against.
     * @return The LinkedHashSet containing a list of accessible <code>Setting</code>s.
     */
    public LinkedHashSet<Setting> getAccessibleSettings(Player player) {
        LinkedHashSet<Setting> accessibleSettings = new LinkedHashSet<>();
        for (Setting setting : settings.values()) {
            // Only add settings that the player has permission to see to the HashSet to return
            if (player.hasPermission(setting.getPermission())) {
                accessibleSettings.add(setting);
            }
        }
        return accessibleSettings;
    }

    /**
     * Categorizes the given settings.
     *
     * @param settings The settings to categorize.
     * @return The settings, sorted by category.
     */
    public EnumMap<SettingCategory, LinkedHashSet<Setting>> categorizeSettings(LinkedHashSet<Setting> settings) {
        EnumMap<SettingCategory, LinkedHashSet<Setting>> categorized = new EnumMap<>(SettingCategory.class);

        for (Setting setting : settings) {
            if (!categorized.containsKey(setting.getCategory())) {
                categorized.put(setting.getCategory(), new LinkedHashSet<>());
            }
            categorized.get(setting.getCategory()).add(setting);
        }
        return categorized;
    }

}
