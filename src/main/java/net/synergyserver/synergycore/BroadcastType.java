package net.synergyserver.synergycore;

/**
 * Types of broadcasts that can be sent.
 */
public enum BroadcastType {
    // A broadcast initiated by a command like /bc
    COMMAND,
    // A broadcast that is deemed to be an important announcement
    ANNOUNCEMENT,
    // A broadcast that is on a schedule (like the ones right before the automated restart)
    SCHEDULED,
    // A broadcast that is on an automatic rotation schedule
    ROTATING,
    // Any other broadcast created by this plugin
    OTHER
}
