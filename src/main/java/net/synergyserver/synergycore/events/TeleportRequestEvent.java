package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.Teleport;
import org.bukkit.event.Cancellable;

/**
 * Represents an event that involves a player's request of a teleport.
 */
public abstract class TeleportRequestEvent extends TeleportEvent implements Cancellable {

    /**
     * Creates a new <code>TeleportRequestEvent</code> with the given <code>Teleport</code>.
     *
     * @param teleport The <code>Teleport</code> of this event.
     */
    public TeleportRequestEvent(Teleport teleport) {
        super(teleport);
    }

}
