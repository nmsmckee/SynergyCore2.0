package net.synergyserver.synergycore;

import org.bukkit.plugin.Plugin;

/**
 * Represents one of the purely custom plugins used by Synergy.
 */
public interface SynergyPlugin extends Plugin {

    /**
     * Gets the aliases of this <code>SynergyPlugin</code>.
     *
     * @return The aliases of this plugin.
     */
    default String[] getAliases() {
        return new String[]{getName()};
    }

    /**
     * Registers the <code>SynCommand</code>s of this <code>SynergyPlugin</code> to be handled by SynergyCore's
     * <code>CommandManager</code>. When adding more commands, make sure that <code>SubCommand</code>s come after
     * their <code>MainCommand</code>.
     */
    default void registerCommands() {}

    /**
     * Registers the <code>Setting</code>s of this <code>SynergyPlugin</code> to be handled by SynergyCore's
     * <code>SettingManager</code>. All <code>Setting</code>s that are to be registered should be marked as
     * public, static, and final.
     */
    default void registerSettings() {}

    /**
     * Registers the event listeners of this <code>SynergyPlugin</code> using Bukkit's event system.
     */
    default void registerListeners() {}

    /**
     * Maps the classes whose objects will be stored in the database.
     * All classes whose objects are to be stored in the database
     * in a non-binary data format must be mapped.
     */
    default void mapClasses() {}

    /**
     * Called after all plugins have been loaded and enabled.
     */
    default void postLoad() {}

    /**
     * Called when the plugin is being reloaded by the /synreload command.
     */
    default void onReload() {}

}
