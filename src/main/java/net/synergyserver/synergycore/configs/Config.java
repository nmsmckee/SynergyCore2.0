package net.synergyserver.synergycore.configs;

import net.synergyserver.synergycore.SynergyPlugin;

/**
 * Represents an object used to access YAML configuration files in this plugin's directory.
 */
public interface Config {

    /**
     * Loads information from the corresponding YAML file in the
     * given <code>SynergyPlugin</code>'s directory, if it exists.
     *
     * @param plugin The <code>SynergyPlugin</code> to load the file for.
     */
    void load(SynergyPlugin plugin);

    /**
     * Unloads the data of the given <code>SynergyPlugin</code>, if it exists.
     *
     * @param plugin The <code>SynergyPlugin</code> to unload the config of.
     */
    void unload(SynergyPlugin plugin);

    /**
     * Saves information to the corresponding YAML file in the given <code>SynergyPlugin</code>'s directory.
     *
     * @param plugin The <code>SynergyPlugin</code> to save the file for.
     */
    void save(SynergyPlugin plugin);
}
